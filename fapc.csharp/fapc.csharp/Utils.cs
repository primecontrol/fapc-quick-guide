﻿using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.Interfaces;
using OpenQA.Selenium.Appium.MultiTouch;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace fapc.csharp
{
    public static class Utils
    {
        public static string GetJsonToken(this string jsonFileName, string token)
        {
            try
            {
                return JObject.Parse(File.ReadAllText(jsonFileName)).SelectToken(token).Value<string>();
            }
            catch (Exception e)
            {
                Hooks.Fail($"Retornar valor para o token: {token}. " + File.ReadAllText(jsonFileName) + $"{e.Message} {e.InnerException} {e.StackTrace}");

                return "";
            }
        }

        public static void SwitchToFrame(this RemoteWebDriver driver, string[] frames, int timeout = Hooks.DefaultTimeout)
        {
            for (int i = 0; i < timeout; i++)
            {
                try
                {
                    foreach (var f in frames)
                    {
                        driver.SwitchTo().Frame(f);
                    }

                    return;
                }
                catch
                {
                    driver.SwitchTo().DefaultContent();
                    Thread.Sleep(1000);
                }
            }
        }

        public static void JsScrollIntoView(this By by, int timeout = Hooks.DefaultTimeout, bool failIfNotExists = false)
        {
            var element = by.Wait(timeout, failIfNotExists);

            if (element != null)
            {
                Hooks.Checkpoint(true, $"Scroll Into View", false, false);

                IJavaScriptExecutor js = Hooks.Driver;
                js.ExecuteScript("arguments[0].scrollIntoView(true);", element);
            }
        }

        public static void JsHidden(this By by, int timeout = Hooks.DefaultTimeout, bool failIfNotExists = false)
        {
            var element = by.Wait(timeout, failIfNotExists);

            if (element != null)
            {
                Hooks.Checkpoint(true, $"Hidden", false, false);

                IJavaScriptExecutor js = Hooks.Driver;
                js.ExecuteScript("arguments[0].setAttribute(arguments[1], arguments[2]);", element, "style", "visibility: hidden;");
            }
        }

        public static void JsSetAttribute(this By by, string attributeName, string attributeValue, int timeout = Hooks.DefaultTimeout, bool failIfNotExists = false)
        {
            var element = by.Wait(timeout, failIfNotExists);

            if (element != null)
            {
                Hooks.Checkpoint(true, $"Set Attributte: {attributeName} {attributeValue}", false, false);

                IJavaScriptExecutor js = Hooks.Driver;
                js.ExecuteScript("arguments[0].setAttribute(arguments[1], arguments[2]);", element, attributeName, attributeValue);
            }
        }

        public static void LongPress(this By by, int timeout = Hooks.DefaultTimeout, bool failIfNotExists = true)
        {
            var element = by.Wait(timeout, failIfNotExists);

            if (element != null)
            {
                new TouchAction((IPerformsTouchActions)Hooks.Driver).LongPress(element).Perform();
            }
        }

        public static void SenKeysNext(this AndroidDriver<AndroidElement> androidDriver)
        {
            androidDriver.ExecuteScript("mobile: performEditorAction", new Dictionary<string, string> { { "action", "next" } });
        }

        public static IWebElement Wait(this By by, int timeout = Hooks.DefaultTimeout, bool failIfNotExists = true)
        {
            for (int i = 0; i < timeout; i++)
            {
                try
                {
                    if (Hooks.Driver.FindElement(by).Displayed || Hooks.Driver.FindElement(by).Enabled)
                    {
                        Hooks.Checkpoint(true, $"Elemento encontrado pelo localizador {by}");

                        return Hooks.Driver.FindElement(by);
                    }
                }
                catch
                {
                    Thread.Sleep(1000);
                }
            }

            if (failIfNotExists)
            {
                Hooks.Checkpoint(false, $"Elemento não encontrado pelo localizador {by}");
            }

            return null;
        }

        public static void Click(this By by, int timeout = Hooks.DefaultTimeout, bool failIfNotExists = true)
        {
            try
            {
                Hooks.AndroidDriver.HideKeyboard();
            }
            catch { }

            var element = by.Wait(timeout, failIfNotExists);

            if (element != null)
            {
                IJavaScriptExecutor js = Hooks.Driver;
                js.ExecuteScript("arguments[0].scrollIntoView(true);", element);

                Hooks.Checkpoint(true, $"Click", false, false);

                element.Click();
            }
        }

        public static void SendKeys(this By by, string text, int timeout = Hooks.DefaultTimeout, bool failIfNotExists = true)
        {
            var element = by.Wait(timeout, failIfNotExists);

            if (element != null)
            {
                Hooks.Checkpoint(true, $"SendKeys: {text}", false, true);

                IJavaScriptExecutor js = Hooks.Driver;
                js.ExecuteScript("arguments[0].scrollIntoView(true);", element);

                element.SendKeys(text);
            }
        }

        public static string Text(this By by, int timeout = Hooks.DefaultTimeout, bool failIfNotExists = true)
        {
            var element = by.Wait(timeout, failIfNotExists);

            if (element != null)
            {
                return element.Text;
            }

            return "";
        }
    }
}