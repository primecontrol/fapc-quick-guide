﻿using fapc.csharp.PageObjects.Web;
using fapc.csharp.Steps.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace fapc.csharp.Features.Web
{
    [TestClass]
    public class LoginFeature : Hooks
{
        [TestMethod]
        public void Login()
        {
            var driver = new ChromeDriver();
            var email = driver.FindElement(By.Id("email"));
            var password = driver.FindElement(By.Id("password"));
            var signIn = driver.FindElement(By.Id("button"));

            email.SendKeys("email@email.com");
            password.SendKeys("Passw0rd");
            signIn.Click();
        }

        [TestMethod]
        public void LoginPom()
        {
            var driver = new ChromeDriver();
            var email = driver.FindElement(LoginPage.Email);
            var password = driver.FindElement(LoginPage.Password);
            var signIn = driver.FindElement(LoginPage.SignIn);

            email.SendKeys("email@email.com");
            password.SendKeys("Passw0rd");
            signIn.Click();
        }

        [TestMethod]
        public void LoginPomSteps()
        {
            SetDriverType(DriverType.WEB, FrontEnd.CHROME, "WebConfig");

            LoginSteps.SetEmail("email@email.com");
            LoginSteps.SetPassword("Passw0rd");
            LoginSteps.ClickSignIn();
        }

        [TestMethod]
        public void LoginPomStepsFinal()
        {
            LoginSteps.LoginPomStepsFinal("email@email.com", "Passw0rd");
        }
    }
}