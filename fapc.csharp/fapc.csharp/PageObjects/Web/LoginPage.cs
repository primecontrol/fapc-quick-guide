﻿using OpenQA.Selenium;

namespace fapc.csharp.PageObjects.Web
{
    public class LoginPage
    {
        public static By Email = By.Id("email");
        public static By Password = By.Id("password");
        public static By SignIn = By.Id("button");
    }
}