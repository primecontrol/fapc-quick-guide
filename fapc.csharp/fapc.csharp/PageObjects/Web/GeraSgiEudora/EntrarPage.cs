﻿using OpenQA.Selenium;

namespace fapc.csharp.PageObjects.Web.GeraSgiEudora
{
    public class EntrarPage
    {
        public static By Usuario = By.Name("txtUsuario$T2");
        public static By Senha = By.Name("txtSenha$T2");
        public static By Entrar = By.Id("btnLogin_btn");
    }
}