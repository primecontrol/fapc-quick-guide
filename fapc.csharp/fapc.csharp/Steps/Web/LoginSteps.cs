﻿using fapc.csharp.PageObjects.Web;

namespace fapc.csharp.Steps.Web
{
    public class LoginSteps : Hooks
    {
        public static void SetEmail(string text)
        {
            LoginPage.Email.SendKeys(text);
        }

        public static void SetPassword(string text)
        {
            LoginPage.Password.SendKeys(text);
        }

        public static void ClickSignIn()
        {
            LoginPage.Password.Click();
        }

        public static void LoginPomStepsFinal(string email, string password)
        {
            SetDriverType(DriverType.WEB, FrontEnd.CHROME, "WebConfig");

            SetEmail(email);
            SetPassword(password);
            ClickSignIn();
        }
    }
}