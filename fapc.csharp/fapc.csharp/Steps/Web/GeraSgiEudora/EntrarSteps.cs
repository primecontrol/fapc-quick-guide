﻿using fapc.csharp.PageObjects.Web.GeraSgiEudora;

namespace fapc.csharp.Steps.Web.GeraSgiEudora
{
    public class EntrarSteps : Hooks
    {
        public static void SetUsuario(string text)
        {
            Logger = $"Set Usuario: {text}";
            EntrarPage.Usuario.SendKeys(text);
        }

        public static void SetSenha(string text)
        {
            Logger = $"Set Senha: {text}";
            EntrarPage.Senha.SendKeys(text);
        }

        public static void ClickEntrar()
        {
            Logger = "Click Entrar";
            EntrarPage.Entrar.Click();
        }

        public static void Login(string usuario = "5580056", string senha = "Prime@123")
        {
            SetDriverType(DriverType.WEB, FrontEnd.CHROME, "WebConfig.json");

            SetUsuario(usuario);
            SetSenha(senha);
            ClickEntrar();

            Checkpoint(AguardarAcaoSteps.CheckpointProfileMenu(), "Checkpoint", false);
        }
    }
}