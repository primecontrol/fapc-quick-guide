﻿using fapc.csharp.PageObjects.Web.GeraSgiEudora;

namespace fapc.csharp.Steps.Web.GeraSgiEudora
{
    public class AguardarAcaoSteps : Hooks
    {
        public static bool CheckpointProfileMenu()
        {
            Logger = "Chekcpoint Profile Menu";
            return AguardarAcaoPage.ProfileMenu.Wait(DefaultTimeout, false) != null;
        }
    }
}